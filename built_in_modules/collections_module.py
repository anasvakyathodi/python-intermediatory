import collections

# # creating a named tuple
# player = collections.namedtuple("Player",["name","age","team"])

# p1 = player("Anz","22","Brazil")
# print(p1.name)

# creating ordered dictionary
d1 = collections.OrderedDict()
d1["name"] = "Anz"
d1["age"] = "22"
d1["team"] = "Brazil"
print(d1["name"])

# creating an efficient queue
q = collections.deque([12,34,56,76])
# append on left
q.appendleft(15)
# append on right
q.append(40)
# pop from right
q.pop()
# pop from left
q.popleft()