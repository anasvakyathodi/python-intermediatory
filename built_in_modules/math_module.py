import math

print(math.pi) # pi value

print(math.e) # euler's number

print(math.degrees()) # radian to degree

print(math.radians()) # degrees to radian

print(math.sin()) # sin

print(math.cos()) # cos

print(math.tan()) # tan

print(math.log(10)) # log

print(math.log10()) # base 10 logarithm

print(math.pow(4,2)) # 4^2

print(math.sqrt(100)) # square root

print(math.ceil(5.59)) # smallest integer >= given value

print(math.foor(5.59)) # largest integer <= given value



