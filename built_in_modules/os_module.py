import os

# create a directory by passing directory name along with path
os.mkdir("./built_in_modules/test")

# change directory by passing directory along with path
os.chdir("./test")

# get current working directory
os.getcwd()

# remove directory
os.rmdir("./built_in_modules/test")

# list directories
os.listdir()