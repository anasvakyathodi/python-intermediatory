import random

# prints random float number between 0.0 and 1.0
print(random.random())

# prints random integer number between given values
print(random.randint(1,100))

# prints random integer number between given values and giving intervals between numbers 
print(random.randrange(1,10,2)) # here possible values: 1,3,5,7,9


# selecting a random value from given input -- char from string or element from list
print(random.choice("Python"))
print(random.choice([1,2,3,4,5]))

