import statistics

print(statistics.mean([1,2,3,4,5,6,7])) # finding mean

print(statistics.median([1,2,3,4,5,6,7])) # finding median

print(statistics.mode([1,2,3,4,5,6,7,7])) # finding mode

