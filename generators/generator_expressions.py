# creating an iterator using expression
newSquare = (i*i for i in range(7))

print(next(newSquare))
print(next(newSquare))
print(next(newSquare))
print(next(newSquare))
print(next(newSquare))
print(next(newSquare))
print(next(newSquare))

# using expression generator iterations with other built in functions
import math
newSquareSum = sum(i*i for i in range(5)) # output : 30 (0 + 1 + 4 + 9 + 16)
print(newSquareSum)