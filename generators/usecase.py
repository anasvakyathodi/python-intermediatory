# for creating square sequance we can use forloop and yield to create a square number generator

def squareGen(x):
    for i in range(x):
        yield i * i

vals = squareGen(5)
while True:
    try:
        print("Next Value: {}".format(next(vals)))
    except StopIteration:
        # handled stopiteration with break
        break


# another way for handling exception is using for loop
# it will automatically stops when iter item ends
for x in vals:
    print(x)