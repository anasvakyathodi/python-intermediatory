# Using generator function using for loop
def testFun(x):
    for i in range(x):
        yield i

test_vals = testFun(3)
print(next(test_vals))
print(next(test_vals))
print(next(test_vals))
# after the limit it will raise exception: stop iteration