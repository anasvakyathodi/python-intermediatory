# yield can be used for making iterators
# Example

def test_fun():
    print("First Variable")
    yield 12

    print("Second Variable")
    yield 24

    print("Third Variable")
    yield 36

# can be iterated using next()
vals = test_fun()
print(next(vals))
print(next(vals))
print(next(vals))

###############################################################################
# we can use return for stopping iterations
def test_fun():
    print("First Variable")
    yield 12
    
    # this will raise an exception while using next after returning 12
    return 
    print("Second Variable")
    yield 24

    print("Third Variable")
    yield 36

# can be iterated using next()
vals = test_fun()
print(next(vals))
print(next(vals))
print(next(vals))