# for tuples
test_tuple = ("anas","shyma","akshay","jinan")
for i in test_tuple:
    print(i)
# for string
test_tuple = "shyma"
tuple_iter = iter(test_tuple)
for i in tuple_iter:
    print(i)