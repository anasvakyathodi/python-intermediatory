# for tuples
test_tuple = ("anas","shyma","akshay","jinan")
tuple_iter = iter(test_tuple)
print(next(tuple_iter))
print(next(tuple_iter))
print(next(tuple_iter))
print(next(tuple_iter))

# for string
test_tuple = "shyma"
tuple_iter = iter(test_tuple)
print(next(tuple_iter))
print(next(tuple_iter))
print(next(tuple_iter))
print(next(tuple_iter))
print(next(tuple_iter))

# counter function

class Counting:
    def __iter__(self):
        self.n = 1
        return self
    def __next__(self):
        # add an exception
        if self.n <= 19:
            x = self.n
            self.n += 1
            return x
        else:
            raise StopIteration

new_obj = Counting()
new_count = iter(new_obj)

print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))
print(next(new_count))