import re
testMsg = "Hello Programming World of Python"
x = re.search("^Hello.*Python$",testMsg)
if x:
    print("String Matches!")
else:
    print("Not Match")


# find all -- returns list containing all matches
testMsg = "Hello Hello world"
x = re.findall(r"\bH\w+",testMsg)
print(x)

# searching for first white space
x = re.search("\s",testMsg)
print(x.start())

# split word using a match 
x = re.split("\s",testMsg)
# control number of occurance in split
x = re.split("\s",testMsg,1)
print(x)


# replacing character using sub
x = re.sub("\s","$",testMsg)
print(x)

# span method to find start and end of a match
x = re.search(r"\bH\w+",testMsg)
print(x.span())
# return the string passed into function
print(x.string)
# return the word matching the condition
print(x.group())