# this value will be available everywehere inside this file
x = 100
# inside function
def test_fun():
    print(x)

test_fun()
# outside function
print(x)

###################################################################

# reassigning global variable from a function
x = 100
def test_fun():
    # referencing global variable
    global x
    x = 50


print(x)
test_fun()
print(x)
