# reassigning global variable from a function
x = 100
def test_fun():
    x = 50
    print(x)

# both variables treated seperately
test_fun()
print(x)
