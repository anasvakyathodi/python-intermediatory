number = 26
new_msg = "This number output is {}"
print(new_msg.format(number))
# print(new_msg.format(56))

# adding floating points
new_msg = "This number output is {:.3f}"
print(new_msg.format(number))


# multiple variable formatting
new_msg = "My age is {} and my height is {:.2f} and weight is {:.2f}"
print(new_msg.format(22,168,57.2))
