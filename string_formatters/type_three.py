# formatting using named indexes
new_msg = "Hey {name}, How are you?"
print(new_msg.format(name="Anz"))


# more than one indices
new_msg = "Hey {name}, Iam {age} years old"
print(new_msg.format(name="Anz",age="23"))
